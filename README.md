# Notes Web Application

This is a simple web application for creating, editing, and deleting notes. It is built with Laravel v9 for the backend and uses MySQL for the database. The frontend is built with Blade templates and JavaScript for AJAX operations.

## Features

- User Registration and Login
- Create, Read, Update, Delete (CRUD) operations for notes
- Secure user authentication with Laravel Sanctum
- Basic styling with Bootstrap
- AJAX for seamless CRUD operations without page reload

## Installation

### Prerequisites

- PHP >= 8.0
- Composer
- MySQL
- Node.js & npm

### Steps

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/wcby/pca.git
   cd pca
   composer install
   npm install
   cp .env.example .env
   php artisan key:generate
   
2. Set up your database in the .env file:

DB_DATABASE=your_database
DB_USERNAME=your_username
DB_PASSWORD=your_password

3. Run the migrations
   ```bash
   php artisan migrate
   php artisan serve
   npm run dev

4. API Endpoints
   POST /api/register - Register a new user
   POST /api/login - Login user
   GET /api/notes - Get all notes for the authenticated user
   POST /api/notes - Create a new note
   PUT /api/notes/{id} - Update an existing note
   DELETE /api/notes/{id} - Delete a note

5. Frontend Pages
   /login - Login page
   /register - Registration page
   /notes - Main page showing the user's notes
   /notes/create - Page to create a new note
   /notes/{id}/edit - Page to edit an existing note
