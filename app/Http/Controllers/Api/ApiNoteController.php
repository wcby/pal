<?php
namespace App\Http\Controllers\Api;

use App\Models\Note;
use Illuminate\Http\Request;
use App\Http\Resources\NoteResource;

class ApiNoteController extends Controller
{
    public function index(Request $request)
    {
        return NoteResource::collection($request->user()->notes);
    }

    public function store(Request $request)
    {
        $note = $request->user()->notes()->create($request->only(['title', 'content']));
        return new NoteResource($note);
    }

    public function update(Request $request, Note $note)
    {
        $note->update($request->only(['title', 'content']));
        return new NoteResource($note);
    }

    public function destroy(Note $note)
    {
        $note->delete();
        return response()->json(null, 204);
    }
}
