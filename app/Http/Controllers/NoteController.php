<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NoteController extends Controller
{
    public function index()
    {
        $notes = Note::where('user_id', auth()->id())->get();
        return view('notes.index', compact('notes'));
    }

    public function create()
    {
        return view('notes.form');
    }
}
