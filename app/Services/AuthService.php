<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    public function register($data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        return $user->createToken('auth_token')->plainTextToken;
    }

    public function login($data)
    {
        if (!auth()->attempt($data)) {
            return false;
        }

        $user = User::where('email', $data['email'])->firstOrFail();
        return $user->createToken('auth_token')->plainTextToken;
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
    }
}
