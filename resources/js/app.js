import './bootstrap';

$(document).ready(function() {
    $('#registerForm').submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: '/api/auth/register',
            method: 'POST',
            data: $(this).serialize(),
            success: function(response) {
                localStorage.setItem('token', response.access_token);
                window.location.href = '/notes';
            },
            error: function(response) {
                alert('Registration failed');
            }
        });
    });

    $('#loginForm').submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: '/api/auth/login',
            method: 'POST',
            data: $(this).serialize(),
            success: function(response) {
                localStorage.setItem('token', response.access_token);
                window.location.href = '/notes';
            },
            error: function(response) {
                alert('Login failed');
            }
        });
    });

    $('#createNoteBtn').click(function() {
        window.location.href = '/notes/create';
    });

    $('#noteForm').submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: '/api/notes',
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            data: $(this).serialize(),
            success: function(response) {
                window.location.href = '/notes';
            },
            error: function(response) {
                alert('Failed to save note');
            }
        });
    });

    if (window.location.pathname === '/notes') {
        $.ajax({
            url: '/api/notes',
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            success: function(response) {
                $('#notesList').html('');
                response.data.forEach(function(note) {
                    $('#notesList').append('<div>' + note.title + '</div>');
                });
            },
            error: function(response) {
                alert('Failed to load notes');
            }
        });
    }

    $('#logoutBtn').click(function() {
        $.ajax({
            url: '/api/logout',
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            success: function(response) {
                localStorage.removeItem('token');
                window.location.href = '/login';
            },
            error: function(response) {
                alert('Logout failed');
            }
        });
    });

    $('button[data-toggle="password"]').click(function() {
        var target = $(this).data('target');
        togglePasswordVisibility(target);
    });
});


