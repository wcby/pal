@extends('layouts.app')

@section('content')
    <div class="container">
        <button id="createNoteBtn" class="btn btn-primary mb-3">Создать заметку</button>
        <div id="notesList"></div>
    </div>
@endsection
