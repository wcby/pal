@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Войти</h2>
                    <form id="loginForm">
                        @csrf
                        <div class="mb-3">
                            <label for="loginEmail" class="form-label">Email</label>
                            <input type="email" class="form-control" id="loginEmail" name="email">
                        </div>
                        <div class="mb-3">
                            <label for="loginPassword" class="form-label">Пароль</label>
                            <input type="password" class="form-control" id="loginPassword" name="password">
                        </div>
                        <button type="submit" class="btn btn-primary">Войти</button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h2>Регистрация</h2>
                    <form id="registerForm">
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-label">Имя</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="mb-3">
                            <label for="registerEmail" class="form-label">Email</label>
                            <input type="email" class="form-control" id="registerEmail" name="email">
                        </div>
                        <div class="mb-3">
                            <label for="registerPassword" class="form-label">Пароль</label>
                            <input type="password" class="form-control" id="registerPassword" name="password">
                        </div>
                        <div class="mb-3">
                            <label for="registerPassword_confirmation" class="form-label">Повторите пароль</label>
                            <input type="password" class="form-control" id="registerPassword_confirmation" name="password_confirmation">
                        </div>
                        <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
